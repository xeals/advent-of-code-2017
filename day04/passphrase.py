#!/usr/bin/env python3
# Counts the number of valid passphrases

valid = 0

def has_dup(line):
    for word in line:
        if line.count(word) > 1:
            return True
    print(line)
    return False

def has_anagram(line):
    return has_dup(list(map(lambda w: sorted(w), line)))

with open('input') as f:
    for line in f.readlines():
        row = line[:-1].split()

        if not (has_dup(row) or has_anagram(row)):
            valid += 1

print(valid)
