#!/usr/bin/env python3
# Sums each line's difference between the largest and smallest numbers

rows = []

def has_divisor(i, rem_row):
    for j in rem_row:
        if i == j:
            continue
        elif i % j == 0:
            app = i // j
        elif j % i == 0:
            app = j // i
        else:
            continue

        rows.append(app)
        return True


with open('input') as f:
    for line in f.readlines():
        row = line[:-1].split()
        nums = list(map(lambda c: int(c), row))

        # PART 1
        # rows.append(max(nums) - min(nums))

        # PART 2
        for i in nums:
            if has_divisor(i, nums):
                break

print(sum(rows))
