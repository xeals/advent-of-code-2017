#!/usr/bin/env python3
# Read a captcha and sum subsequent numbers (circularly)

with open('input') as f:
    input = f.read()

acc = 0
input = input[:-1]

# PART 1

# for i, n in enumerate(input):
#     if i < len(input) - 1 and n == input[i + 1]:
#         acc += int(n)

# if input[len(input) - 1] == input[0]:
#     acc += int(input[0])


# PART 2
for i, n in enumerate(input):
    if i < len(input) // 2:
        mod = len(input) // 2
    else:
        mod = len(input) // -2

    if n == input[i + mod]:
        acc += int(n)

print(acc)
