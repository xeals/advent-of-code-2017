#!/usr/bin/env python3
# Escape a maze of instructions

with open('t1') as f:
    maze = f.read().split()
    maze = list(map(lambda x: int(x), maze))

jumps = 0
loc = 0

# PART 1
# def inc(steps):
#     return 1

# PART 2
def inc(steps):
    if steps >= 3:
        return -1
    else:
        return 1

while True:
    try:
        print("Trying", loc)
        steps = maze[loc]
        print("  ... has steps", steps)

        maze[loc] += inc(steps)
        loc += steps
        jumps += 1

    except IndexError as e:
        print("EXITED:")
        print(str(jumps))
        break
