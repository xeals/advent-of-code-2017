use std::fs::File;
use std::io::{self, Read};
use std::str::Chars;

fn main() {
    let input = open_input("input").unwrap();
    let mut stream = input.chars();

    match stream.next() {
        Some('{') => {
            let pr = parse_group(&mut stream, 1);
            println!("\nScore: {}\nGarbage: {}", pr.0, pr.1);
        },
        _ => unreachable!(),
    }
}

fn parse_group(stream: &mut Chars, size: usize) -> (usize, usize) {
    let mut _size = size;
    let mut _garb = 0;

    loop {
        let c = stream.next();
        println!("{:?}", c);
        match c {
            Some('{') => {
                let pr = parse_group(stream, size + 1);
                _size += pr.0;
                _garb += pr.1;
            },
            Some('}') => { println!("Leaving group {}", size); break },
            Some('<') => { println!("Chewing inside group {}", size); _garb += chew_garbage(stream) },
            Some('!') => { stream.next(); },
            Some(',') => (),
            Some(a) => println!("Expected a control character, found `{}`.", a),
            None => { println!("Unexpected end of stream in group {}.", size); break }
        }
    }

    (_size, _garb)
}

fn chew_garbage(stream: &mut Chars) -> usize {
    let mut size = 0;

    loop {
        let c = stream.next();
        println!("{:?}", c);
        match c {
            Some('>') => { println!("Finished garbage."); break },
            Some('!') => { stream.next(); },
            Some(_) => size += 1,
            None => { println!("Unexpected end of stream in garbage."); break }
        }
    }

    size
}

fn open_input(file: &str) -> io::Result<String> {
    let mut buf = String::new();
    let mut file = File::open(file)?;
    file.read_to_string(&mut buf)?;
    Ok(String::from(buf.trim()))
}
