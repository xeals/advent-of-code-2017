// Moves items around in memory until a previously-detected state occurs.
// Commented lines are for part 1.

//use std::collections::HashSet;
use std::collections::HashMap;
use std::fs::File;
use std::io::{self, Read};

fn main() {
    let mut memory = open_input("input").unwrap();

    //let mut prev = HashSet::new();
    let mut prev = HashMap::new();

    let mut count = 0;
    let cycles;

    loop {
        //if !prev.insert(memory.clone()) { break }

        match prev.insert(memory.clone(), count) {
            None => (),
            Some(last_value) => {
                cycles = count - last_value;
                break
            }
        }

        let (n, mut idx) = max(&memory);
        memory[idx] = 0;

        println!("MEMORY: {:?}", memory);

        for _ in 0..n {
            if idx >= memory.len() - 1 {
                idx = 0;
            } else {
                idx += 1;
            }
            memory[idx] += 1;
        }

        count += 1;
    }

    println!("{} cycles total; {} since last sighting", count, cycles);
}

fn open_input(file: &str) -> io::Result<Vec<usize>> {
    let mut buf = String::new();
    let mut file = File::open(file)?;
    file.read_to_string(&mut buf)?;
    Ok(buf.split_whitespace().map(|x| x.parse::<usize>().expect("Not a number")).collect::<Vec<_>>())
}

/// Finds the maximum number in a vector, and the position of that number.
fn max(vec: &Vec<usize>) -> (usize, usize) {
    let mut it = vec.iter();
    let m = it.clone().max().unwrap();
    let p = it.position(|x| x == m).unwrap();

    (*m, p)
}
